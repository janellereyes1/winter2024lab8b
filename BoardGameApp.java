import java.util.Scanner;

public class BoardGameApp {
	public static void main(String[] args) {
		System.out.println("Hello! Welcome to the Board Game!! :D");
		Board myBoard = new Board();
		
		int numCastles = 7;
		int turns = 0;
		
		while(numCastles > 0 && turns < 8) {
			System.out.println(myBoard);
			System.out.println("You have: " + numCastles + " castle(s)");
			System.out.println("You're on turn: " + turns + "/8" + "\n");
		
			Scanner reader = new Scanner(System.in);
		
			System.out.println("Enter which row you want to place your token!(0-4)");
			int row = reader.nextInt();
			
			System.out.println("Enter which column you want to place your token!(0-4)");
			int col = reader.nextInt();
			System.out.println();
		
			int placedToken = myBoard.placeToken(row, col);
			
			while(placedToken < 0) {
				System.out.println("Whoopsie! Please re-enter your inputs :)");
				
				System.out.println("Enter which row you want to place your token!(0-2)");
				row = reader.nextInt();
			
				System.out.println("Enter which column you want to place your token!(0-2)");
				col = reader.nextInt();
				System.out.println();
				
				placedToken = myBoard.placeToken(row, col);
			}
			
			if(placedToken == 1) {
				System.out.println("There is a wall at this position!");
				turns++;
			}
			
			if(placedToken == 0) {
				System.out.println("The castle was successfully placed! :D");
				turns++;
				numCastles--;
			}
		}
		System.out.println(myBoard);
		if(numCastles == 0) {
			System.out.println("You won!!!");
		}
		else {
			System.out.println("You lost:(");
		}
	}
}