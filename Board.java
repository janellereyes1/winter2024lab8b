import java.util.Random;

public class Board {
	Tile[][] grid;
	final int boardSize = 5;
	
	public Board() {
		this.grid = new Tile[boardSize][boardSize];
		
		Random rng = new Random();
		for(int i = 0; i < boardSize; i++) {
			int randIndex = rng.nextInt(boardSize);
			for(int j = 0; j < boardSize; j++) {
				if(j == randIndex) {
					grid[i][j] = Tile.HIDDEN_WALL;
				}
				else {
					grid[i][j] = Tile.BLANK;
				}
			}
		}
	}
	
	public String toString() {
		String board = "";
		for(int i = 0; i < boardSize; i++) {
			for(int j = 0; j < boardSize; j++) {
				board += this.grid[i][j].getName() + " ";
			}
			board += "\n";
		}
		return board;
	}
	
	public int placeToken(int row, int col) {
		int token = 0;
		if(row < 0 || row >= boardSize || col < 0 || col >= boardSize) {
			token = -2;
		}
		if(grid[row][col] == Tile.CASTLE || grid[row][col] == Tile.WALL) {
			token = -1;
		}
		if(grid[row][col] == Tile.HIDDEN_WALL) {
			grid[row][col] = Tile.WALL;
			token = 1; 
		}
		if(grid[row][col] == Tile.BLANK) {
			grid[row][col] = Tile.CASTLE;
			token = 0;
		}
		return token;
	}
}